import threading
import json
import random
import time
import os


class IoTDevice(threading.Thread):
    def __init__(self, device_id, delta_t, max_info_size):
        super().__init__()
        self.device_id = device_id
        self.delta_t = delta_t
        self.max_info_size = max_info_size

    def run(self):
        while True:
            timestamp =  int(time.time())
            info_size = random.randint(1, self.max_info_size)
            info = ''.join(random.choices('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', k=info_size))
            data = {
                'Timestamp': timestamp,
                'Values': info
            }
            json_data = json.dumps(data)

            print(f"Dispositivo {self.device_id}: {json_data}")

            time.sleep(self.delta_t)


# Obtener los parámetros de las variables de entorno
num_devices = int(os.environ.get('NUM_DEVICES', 5))
delta_t = int(os.environ.get('DELTA_T', 1))
max_info_size = int(os.environ.get('MAX_INFO_SIZE', 10))

# Crear y ejecutar los dispositivos IoT
devices = []
for i in range(num_devices):
    device = IoTDevice(device_id=i+1, delta_t=delta_t, max_info_size=max_info_size)
    devices.append(device)
    device.start()

