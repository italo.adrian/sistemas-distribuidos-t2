# Sistemas Distribuidos T2



## Comenzando

Esta tarea está separada por 3 partes, donde:

- Parte1: Punto2 de la tarea
- Parte2: Punto3 de la tarea
- Parte3: Punto4 de la tarea

Para ejecutar este programa, debe seguir los siguientes pasos.

## Parte 1

Clonar repositorio y montarse en la carpeta **Parte1**

Crear imagen docker

`docker build -t producers .`

Ejecutar docker

`docker run -it -e NUM_DEVICES=5 -e DELTA_T=1 -e MAX_INFO_SIZE=10 producers`

Donde
1. NUM_DEVICES: Corresponde a la cantidad de dispositivos IoT a simular
2. DELTA_T: Correspo.nde al intervalo de tiempo en el que se eniaran los mensajes.
3. MAX_INFO_SIZE: Corresponde al tamaño maximo que puede tener un mensaje, es el tamaño en caracteres (10 caracteres en este caso)

## Parte 2

Clonar repositorio y montarse en la carpeta **Parte2**

Crear imagen docker

`docker build -t producersconsumer .`

Ejecutar docker

`docker run -it -e NUM_DEVICES=3 -e DELTA_T=1 -e MAX_INFO_SIZE=10 -e NUM_CONSUMERS=3 -e MAX_ELEMENTS=5 producersconsumer`

Donde
1. NUM_DEVICES: Corresponde a la cantidad de dispositivos IoT a simular
2. DELTA_T: Correspo.nde al intervalo de tiempo en el que se eniaran los mensajes.
3. MAX_INFO_SIZE: Corresponde al tamaño maximo que puede tener un mensaje, es el tamaño en caracteres (10 caracteres en este caso)
4. NUM_CONSUMERS: Corresponde a la cantidad de dispositivos que reciben la información.
5. MAX_ELEMENTS: Parametro adicional para limitar la cantidad de mensajes para evitar un bucle infinito.

## Parte 3

Clonar repositorio y montarse en la carpeta **Parte3**

Ejecutar docker-compose

`docker run -it --name rabbit-server --network bridge -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=user -p 5672:5672 -p 15672:15672 rabbitmq:3-management`



