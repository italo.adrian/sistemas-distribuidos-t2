import threading
import json
import random
import time
import os
import pika



class IoTDevice(threading.Thread):
    def __init__(self, device_id, delta_t, max_info_size):
        super().__init__()
        self.device_id = device_id
        self.delta_t = delta_t
        self.max_info_size = max_info_size

    def run(self):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq',
                                                                           port='5672',
                                                                           credentials=pika.PlainCredentials('user', 'user')
                                                                           ))
        channel = connection.channel()
        channel.queue_declare(queue="tunelcito")
        
        while True:
            timestamp =  int(time.time())
            info_size = random.randint(self.max_info_size//2, self.max_info_size)
            info = ''.join(random.choices('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', k=info_size))
            data = {
                'Timestamp': timestamp,
                'Values': info
            }
            json_data = json.dumps(data)

            print(f"Dispositivo {self.device_id}: {json_data}")

            

            channel.basic_publish(exchange='',
                      routing_key='tunelcito',
                      body=json_data)

            time.sleep(self.delta_t)


# Obtener los parámetros de las variables de entorno
num_devices = int(os.environ.get('NUM_DEVICES', 20))
delta_t = int(os.environ.get('DELTA_T', 2))
max_info_size = int(os.environ.get('MAX_INFO_SIZE', 50))

# Crear y ejecutar los dispositivos IoT
devices = []
for i in range(num_devices):
    device = IoTDevice(device_id=i+1, delta_t=delta_t, max_info_size=max_info_size)
    devices.append(device)
    device.start()