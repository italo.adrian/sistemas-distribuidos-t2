# import threading
# import json
# import pika
# import os

# class MessageConsumer(threading.Thread):
#     def __init__(self, consumer_id):
#         super().__init__()
#         self.consumer_id = consumer_id

#     def run(self):
#         connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost',
#                                                                        port='5672',
#                                                                        credentials=pika.PlainCredentials('guest', 'guest')))
#         channel = connection.channel()
#         channel.queue_declare(queue="tunelcito")

#         def callback(ch, method, properties, body):
#             data = json.loads(body.decode())
#             print(f"Consumer {self.consumer_id} received message: {data}")

#         channel.basic_consume(queue="tunelcito", on_message_callback=callback, auto_ack=True)

#         print(f"Consumer {self.consumer_id} started")
#         channel.start_consuming()


# # Obtener el número de consumidores desde una variable de entorno
# num_consumers = int(os.environ.get('NUM_CONSUMERS', 3))

# # Crear y ejecutar los consumidores
# consumers = []
# for i in range(num_consumers):
#     consumer = MessageConsumer(consumer_id=i+1)
#     consumers.append(consumer)
#     consumer.start()

import pika, sys, os

def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq',credentials=pika.PlainCredentials('user', 'user')))
    channel = connection.channel()

    channel.queue_declare(queue='tunelcito')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    channel.basic_consume(queue='tunelcito', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)