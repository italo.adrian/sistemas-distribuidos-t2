import threading
import json
import random
import time
from queue import Queue
import os


class Producer(threading.Thread):
    def __init__(self, device_id, delta_t, max_info_size, data_queue, max_elements):
        super().__init__()
        self.device_id = device_id
        self.delta_t = delta_t
        self.max_info_size = max_info_size
        self.data_queue = data_queue
        self.max_elements = max_elements
        self.generated_elements = 0

    def run(self):
        while self.generated_elements < self.max_elements:
            timestamp = int(time.time())
            info_size = random.randint(1, self.max_info_size)
            info = ''.join(random.choices('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', k=info_size))
            data = {
                'Timestamp': timestamp,
                'Values': info
            }
            json_data = json.dumps(data)

            self.data_queue.put((self.device_id, json_data))

            self.generated_elements += 1
            print(f"Dispositivo {self.device_id}: {json_data}")
            time.sleep(self.delta_t)


class Consumer(threading.Thread):
    def __init__(self, data_queue, max_elements):
        super().__init__()
        self.data_queue = data_queue
        self.max_elements = max_elements
        self.processed_elements = 0

    def run(self):
        while self.processed_elements < self.max_elements:
            device_id, data = self.data_queue.get()
            print(f"Consumer received from Device {device_id}: {data}")

            self.data_queue.task_done()
            self.processed_elements += 1


# Obtener los parámetros de las variables de entorno
num_devices = int(os.environ.get('NUM_DEVICES', 5))
delta_t = int(os.environ.get('DELTA_T', 1))
max_info_size = int(os.environ.get('MAX_INFO_SIZE', 10))
num_consumers = int(os.environ.get('NUM_CONSUMERS', 3))
max_elements = int(os.environ.get('MAX_ELEMENTS', 20))  # Número máximo de elementos a producir y procesar

# Crear cola para compartir datos entre Producers y Consumers
data_queue = Queue()

# Crear y ejecutar los dispositivos IoT (Producers)
devices = []
for i in range(num_devices):
    device = Producer(device_id=i+1, delta_t=delta_t, max_info_size=max_info_size, data_queue=data_queue,
                       max_elements=max_elements)
    devices.append(device)
    device.start()

# Crear y ejecutar los consumidores
consumers = []
for i in range(num_consumers):
    consumer = Consumer(data_queue=data_queue, max_elements=max_elements)
    consumers.append(consumer)
    consumer.start()

# Esperar a que todos los elementos sean producidos y procesados antes de finalizar el programa
for device in devices:
    device.join()

data_queue.join()

